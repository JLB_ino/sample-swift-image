//
//  ViewController.swift
//  Image
//
//  Created by LinuxAcademy on 2016/10/31.
//  Copyright © 2016年 LinuxAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	// ------ プロパティ -----
	// 画像表示部分のオブジェクト
	@IBOutlet weak var imageView: UIImageView!
	// 変換前の画像オブジェクト
	var originalImage: UIImage?
	
	// ----- 関数 -----
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	
	// 関数①：カメラロール呼び出し処理
	@IBAction func handleTap(sender: UITapGestureRecognizer) {
		
		// 1. カメラロールにアクセスできるかを判定
		if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
			
			// 1-1. アクセスできる場合
			// 1-1-1. カメラロールが使用できる場合はUIImagePickerControllerオブジェクトを生成
			let ipc:UIImagePickerController = UIImagePickerController()
			
			// 1-1-2. 上記に値を設定
			ipc.delegate   = self
			ipc.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
			
			// 1-1-3. カメラロールを表示
			self.presentViewController(ipc, animated:true, completion:nil)
		}
	}
	
	
	// 関数②：画像セット処理
	func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
		
		// 1. 変数infoからUIImagePickerControllerOriginalImage型のオブジェクトがあるかを判定
		if info[UIImagePickerControllerOriginalImage] != nil {
			
			// 1-1-1. 対象のオブジェクトをUIImageにダウンキャストして変数imageView内の値に代入
			imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
			
			// 1-1-2. 対象のオブジェクトをUIImageにダウンキャストして変数originalImageに代入
			originalImage   = info[UIImagePickerControllerOriginalImage] as? UIImage
		}
		
		// 2. 前の画面を表示
		picker.dismissViewControllerAnimated(true, completion: nil)
	}
	
	
	// 関数③：画像編集処理1
	@IBAction func valueChanged(sender: UISlider) {
		
		// 1. 変数originalImageの内容を判定
		guard let image = originalImage else {
			
			// 1-1. 存在しない場合は処理を終了
			return
		}
		
		// 2. brightnessImage関数で画像を編集し、画面に表示
		imageView.image = brightnessImage(image, brightness: CGFloat(sender.value))
	}
	
	
	// 関数④：輝度編集処理
	func brightnessImage(srcImage: UIImage, brightness: CGFloat) -> UIImage {
		
		// 1. image: srcImageを引数にCIImageクラスオブジェクトを作成
		let ciImage: CIImage = CIImage(image: srcImage)!;
		
		// 2. options: nilを引数にCIContextクラスオブジェクトを作成
		let ciContext: CIContext = CIContext(options: nil)
		
		// 3. name: "CIColorControls"を引数にCIFilterクラスオブジェクトを作成
		let ciFilter: CIFilter = CIFilter(name: "CIColorControls")!
		
		// 4. CIFilterクラスオブジェクトにkCIInputImageKeyをキーとしてCIImageクラスオブジェクトをセット
		ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
		
		// 5. CIFilterクラスオブジェクトに画像の輝度情報となる変数brightnessをセット
		ciFilter.setValue(brightness, forKey: kCIInputBrightnessKey)
		
		// 6. CIContextクラスオブジェクトのcreateCGImage関数で画像を編集
		let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect: ciFilter.outputImage!.extent)
		
		// 7. 編集した画像をUIImageオブジェクトとして返却
		return UIImage(CGImage: cgimg, scale: 1.0, orientation: UIImageOrientation.Up)
	}
	
	
	// 関数⑤：画像編集処理2
	@IBAction func pushedMonochrome(sender: UIButton) {
		
		// 1. 変数imageViewの値を判定
		guard let image = imageView.image else {
			
			// 1-1. 値がnilであれば処理終了
			return
		}
		
		// 2. モノクロ編集処理関数（次項で作成）を実行し、変数imageView内の値とoriginalImageに代入
		let monochroImage = monochromeImage(image)
		imageView.image = monochroImage
		originalImage   = monochroImage
	}
	
	
	// 関数⑥：モノクロ編集処理
	func monochromeImage(srcImage: UIImage) -> UIImage {
		
		// 1. image: srcImageを引数にCIImageクラスオブジェクトを作成
		let ciImage: CIImage = CIImage(image: srcImage)!;
		
		// 2. options: nilを引数にCIContextクラスオブジェクトを作成
		let ciContext: CIContext = CIContext(options: nil)
		
		// 3. name: "CIMinimumComponent"を引数にCIFilterクラスオブジェクトを作成
		let ciFilter: CIFilter = CIFilter(name: "CIMinimumComponent")!
		
		// 4. CIFilterクラスオブジェクトに編集対象となるCIImageクラスオブジェクトをセット
		ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
		
		// 5. CIContextクラスオブジェクトのcreateCGImage関数で画像を編集
		let cgimg: CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect: ciFilter.outputImage!.extent)
		
		// 6. 編集した画像をUIImageオブジェクトとして返却
		return UIImage(CGImage: cgimg, scale: 1.0, orientation: UIImageOrientation.Up)
	}
	
	
	// 関数⑦：画像保存処理
	@IBAction func save(sender: UIButton) {
		
		// 1. UIImageWriteToSavedPhotosAlbumクラスオブジェクトを作成
		// 第1引数：imageView.image：画像オブジェクト
		// 第2引数：self：保存処理実行後のコールバックオブジェクト
		// 第3引数：コールバック時に実行される関数
		// 第4引数：コールバック時に渡されるポインタ
		UIImageWriteToSavedPhotosAlbum(imageView.image!, self, #selector(ViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
	}
	
	
	// 関数⑧：画像保存後処理
	func image(image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutablePointer<Void>) {
		
		// 1. アラート画面を作成
		let alert: UIAlertController = UIAlertController(title: "完了", message: "カメラロールに保存完了", preferredStyle: UIAlertControllerStyle.Alert)
		
		// 2. 「Cancel」ボタンタップ時の処理を作成
		let canselAction: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) -> Void in print("Cancel") })
		
		// 3. 「OK」ボタンタップ時の処理
		let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in print("OK") })
		
		// 4. 作成した処理をアラート画面に設定
		alert.addAction(canselAction)
		alert.addAction(defaultAction)
		
		// 5. アラート画面を表示
		presentViewController(alert, animated: true, completion: nil)
	}
}